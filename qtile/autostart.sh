#!/bin/sh
/home/arthurhagen/.config/nitrogen/randwallpaper /home/arthurhagen/.config/nitrogen/bg-saved.cfg ~/Pictures/wallpaper/active/ &
nitrogen --restore &
picom &
conky &
kdeconnect-indicator &
discord --start-minimized &
